var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
const Alexa = require('alexa-sdk');

const APP_ID = "amzn1.ask.skill.b580f1fa-d475-413b-b8ee-49bc3f71213c";

const SKILL_NAME = 'urBeats';
const GET_FACT_MESSAGE = "Here's your fact: ";
const HELP_MESSAGE = 'You can say tell me a space fact, or, you can say exit... What can I help you with?';
const HELP_REPROMPT = 'What can I help you with?';
const STOP_MESSAGE = 'Goodbye!';

exports.handler = function(event, context, callback) {
    const alexa = Alexa.handler(event, context, callback);
    alexa.appId = APP_ID // APP_ID is your skill id which can be found in the Amazon developer console where you create the skill.
    alexa.execute();
};

const handlers = {
    'LaunchRequest': function () {
        this.response.speak("Welcome niko");
    },
    'PlayMusicIntent' : function() {
        io.sockets.on('connection', function(socket){
            console.log("new user connected with id <" + socket.id + '>');
            this.response.speak("Stuff is happening!");
        });
    },
    'AMAZON.HelpIntent': function () {
        const speechOutput = HELP_MESSAGE;
        const reprompt = HELP_REPROMPT;

        this.response.speak(speechOutput).listen(reprompt);
        this.emit(':responseReady');
    },
    'AMAZON.CancelIntent': function () {
        this.response.speak(STOP_MESSAGE);
        this.emit(':responseReady');
    },
    'AMAZON.StopIntent': function () {
        this.response.speak(STOP_MESSAGE);
        this.emit(':responseReady');
    },
};

var port = process.env.port || 3030;

app.get('/',function(req, res){
    res.send("Welcome!");
});

http.listen(port,function(){
    console.log("App has started with port: ", port);
});